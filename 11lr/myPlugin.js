var gameInfo = {
	countCube: 1,
	localScorePlayer: 0,
	localScoreComputer: 0,
	globalScorePlayer: 0,
	globalScoreComputer: 0

};


(function($){
      
    $.fn.myPlugin = function() {

		this.css("position", "relative").css("width", "1060px").css("background-color", "#aaaaaa").css("border-radius", "15px").css("display", "flex");
		var menu = $('<div />').appendTo(this).css("background-color", "#eeeeee").css("border-radius", "15px").css("padding", "15px").css("margin", "15px").css("width", "300px");
		var field = $('<div />').appendTo(this).css("background-color", "#eeeeee").css("border-radius", "15px").css("padding", "15px").css("width", "700px").css("margin", "15px");
		var fieldTitle = $('<div />').appendTo(this);
		var fieldTitleMain = $('<div />').appendTo(this).css("display", "flex");
		var fieldTitleLeft = $('<div />').css("width", "50%");
		var fieldTitleRight = $('<div />').css("width", "50%");
		var gameField = $('<div />').appendTo(this).css("display", "flex");
		field.append(fieldTitle).append(fieldTitleMain.append(fieldTitleLeft).append(fieldTitleRight));
		field.append(gameField);


		fieldTitle.append($('<h3 />', {
			text: 'Игровое поле',
			style: 'text-align: center'
		}))
		fieldTitleLeft.append($('<p />', {
			text: 'Вы',
			style: 'text-align: center'
		}))
		fieldTitleRight.append($('<p />', {
			text: 'Компьютер',
			style: 'text-align: center'
		}))


		menu.append($('<h1 />', {
            text: 'Кубики',
            style: 'text-align: center' 
        }));
        menu.append($('<hr />'));

        menu.append($('<h3 />', {
            text: 'Настройка',
            style: 'text-align: center' 
        }));  
        menu.append($('<p />', {
            id: "countCube",
            text: 'Количество кубиков (от 1 до 6): 1',
            style: 'text-align: center' 
        }));
        menu.append($("<p />").append('<button id="btnSub" onclick="requestAddCube();">Добавить 1 кубик</button>'));
        menu.append($("<p />").append('<button id="btnAdd" onclick="requestSubCube();">Убрать 1 кубик</button>'));
        $("#btnSub").css("margin-left", "50px").css("width", "200px").css("border", "none").css("background-color", "#cccccc").css("border-radius", "15px").css("padding", "15px");
        $("#btnAdd").css("margin-left", "50px").css("width", "200px").css("border", "none").css("background-color", "#cccccc").css("border-radius", "15px").css("padding", "15px");
        menu.append($('<hr />'));


		menu.append($('<h3 />', {
            text: 'Счет в игре',
            style: 'text-align: center' 
        }));        
        menu.append($('<p />', {
            id: "gameResult",
            text: '(Вы) 0 : 0 (Компьютер)',
            style: 'text-align: center' 
        }));
        menu.append($('<h3 />', {
            text: 'Счет в раунде',
            style: 'text-align: center' 
        }));  
        menu.append($('<p />', {
            text: '(Раунд идет до 6 побед одной из сторон)',
            style: 'text-align: center' 
        }));    
        menu.append($('<p />', {
            id: "roundResult",
            text: '(Вы) 0 : 0 (Компьютер)',
            style: 'text-align: center' 
        }));
        menu.append($('<hr />'));

        menu.append($('<h3 />', {
            text: 'Управление',
            style: 'text-align: center' 
        }));  
        menu.append($("<p />").appendTo(this).append('<button id="btnStrt" onclick="requestStrt();">Старт</button>'));
        $("#btnStrt").css("margin-left", "50px").css("width", "200px").css("border", "none").css("background-color", "#cccccc").css("border-radius", "15px").css("padding", "15px");
        menu.append($("<p />").appendTo(this).append('<button id="btnSurr" onclick="requestSurr();">Сдаться</button>'));
        $("#btnSurr").css("margin-left", "50px").css("width", "200px").css("border", "none").css("background-color", "#cccccc").css("border-radius", "15px").css("padding", "15px");
        menu.append($("<p />").appendTo(this).append('<button id="btnRst" onclick="reauestRst();">Сброс</button>'));
        $("#btnRst").css("margin-left", "50px").css("width", "200px").css("border", "none").css("background-color", "#cccccc").css("border-radius", "15px").css("padding", "15px");

        gameField.append($('<div />', {
            id: "gameFieldPlayer"
        }).css("width", "50%").css("height", "100%").css("background-color", "#a0ffff").css("border-radius", "15px").css("padding", "15px"));
        gameField.append($('<div />', {
            id: "gameFieldComputer"
        }).css("width", "50%").css("height", "100%")	.css("background-color", "#ffa0a0").css("border-radius", "15px").css("padding", "15px"));
	};

})(jQuery);


function requestAddCube(){
	if(gameInfo.countCube < 6)
		gameInfo.countCube++;
	updateGameInfo();
};

function requestSubCube(){
	if(gameInfo.countCube > 1)
		gameInfo.countCube--;
	updateGameInfo();
};

function requestStrt(){
	hideCubes();
	valuePlayer = 0;
	valueComputer = 0;
	for(var i = 0; i < gameInfo.countCube; i++){
		newValuePlayer = generateRandValue(1 ,6);
		newValueComputer = generateRandValue(1, 6);
		valuePlayer += newValuePlayer;
		valueComputer += newValueComputer;
		showCubes(newValuePlayer, newValueComputer);
	}
	if(valuePlayer > valueComputer)
		if(gameInfo.localScorePlayer < 5)
			gameInfo.localScorePlayer++;
		else {
			gameInfo.localScorePlayer = 0;
			gameInfo.localScoreComputer = 0;
			gameInfo.globalScorePlayer++;
		}
	else if(valuePlayer < valueComputer)
		if(gameInfo.localScoreComputer < 5)
			gameInfo.localScoreComputer++;
		else {
			gameInfo.localScorePlayer = 0;
			gameInfo.localScoreComputer = 0;
			gameInfo.globalScoreComputer++;
		}
	else {
		if(valuePlayer > 4){
			gameInfo.localScorePlayer = 0;
			gameInfo.localScoreComputer = 0;
			gameInfo.globalScorePlayer++;
			gameInfo.globalScoreComputer++;
		} else {
			gameInfo.localScorePlayer++;
			gameInfo.localScoreComputer++;
		}
	}
	updateGameInfo();
};

function requestSurr(){
	gameInfo.localScorePlayer = 0;
	gameInfo.localScoreComputer = 0;
	gameInfo.globalScoreComputer++;
	updateGameInfo();
};

function reauestRst(){
	gameInfo.localScorePlayer = 0;
	gameInfo.localScoreComputer = 0;
	gameInfo.globalScorePlayer = 0;
	gameInfo.globalScoreComputer = 0;
	updateGameInfo();
	hideCubes();
};

function showCubes(valuePlayer, valueComputer){
	$("#gameFieldPlayer").append("<img src='../gallery/cubes/" + valuePlayer + ".png' height='100px'>");
	$("#gameFieldComputer").append("<img src='../gallery/cubes/" + valueComputer + ".png' height='100px'>");
};

function hideCubes(){
	$("#gameFieldPlayer").html("");
	$("#gameFieldComputer").html("");
};

function updateGameInfo() {
	$("#countCube").text("Количество кубиков (от 1 до 6): " + gameInfo.countCube);
	$("#roundResult").text("(Вы) " + gameInfo.localScorePlayer+ " : " + gameInfo.localScoreComputer + " (Компьютер)");
	$("#gameResult").text("(Вы) " + gameInfo.globalScorePlayer + " : " + gameInfo.globalScoreComputer + " (Компьютер)");
};

function generateRandValue(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min + 1)) + min;
};