<!DOCTYPE html>
<html>
<head>
	<title>
		Задание 3-3
	</title>
</head>
<body>
	<form action='<? print $PHP_SELF ?>' method='post'>
		<p> Число N -> <input type='text' name='num' size='5'>
		<p> Действие -> <select name='type' size='1'>
			<option value="1" selected> четные делители
			<option value="2"> нечетные делители
			<option value="3"> простые делители
			<option value="4"> составные делители
		</select>
		<p> <input type='submit' name='but' value='Отправить'>
	</form>

    <?
        if (isset($_POST["but"])) {
            $n = $_POST["num"];

            switch ($_POST["type"]) {
                case 1:
                    for ($i = 1; $i < $n; $i++) {
                        if (isEven($i)) {
                            echo $i . " ";
                        }
                    }
                break;

                case 2:
                    for ($i = 1; $i < $n; $i++) {
                        if (!isEven($i)) {
                            echo $i . " ";
                        }
                    }
                break;

                case 3:
                    echo "<br>Простые:<br>";
                    for ($i = 1; $i < $n; $i++) {
                        if (isPrime($i)) {
                            echo $i . " ";
                        }
                    }
                break;

                case 4:
                    echo "<br>Составные:<br>";
                    for ($i = 1; $i < $n; $i++) {
                        if (!isPrime($i)) {
                            echo $i . " ";
                        }
                    }
                break;
            }
            
        }

        function isPrime($number) {
            for ($i = 2; $i <= $number / 2; $i++) {
                if ($number % $i == 0) {
                    return false;
                }
            }
            return true;
        }

        function isEven($number) {
            if ($number % 2 == 0) {
                return true;
            }
            return false;
        }

    ?>

</body>
</html>