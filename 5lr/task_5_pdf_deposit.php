<?
	require('../lib/tfPdf/tfpdf.php');

	class PDF extends tFPDF {

		function Table($req, $row, $link, $header) {
			$countLine = mysqli_num_rows($req);
			$countCol = 3;

			for($i = 0; $i < $countCol; $i++)
				$sizeCol[] = 0;

			for ($i = 0; $i < $countCol; $i++) {
				if(strlen($header[$i]) > $sizeCol[$i])
							$sizeCol[$i] = strlen($header[$i]);
				$data[0][$i] = $header[$i];
			}

			for ($i = 0; $i < $countLine; $i++) {
				for ($j = 0; $j < $countCol; $j++) {
					if(strlen($row[$j]) > $sizeCol[$j])
							$sizeCol[$j] = strlen($row[$j]);
					$data[$i + 1][$j] = $row[$j];
				}
				$row = mysqli_fetch_row($req);
			}

		    for ($i = 0; $i < $countLine + 1; $i++) {
		    	for ($j = 0; $j < $countCol; $j++) {
		    		$this->Cell($sizeCol[$j] + 10, 6, $data[$i][$j], 1);
		    	}
		    	$this->Ln();
			}

		}
	}

	$link = mysqli_connect('localhost', 'u1169180_default', 'c8!_sVJ6') or die ("Невозможно подключиться к серверу");
	mysqli_query($link, 'SET NAMES utf8');
	mysqli_select_db($link, 'u1169180_default') or die ("Нет такой таблицы!");

	$pdf = new PDF();

	$pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
    $pdf->SetFont('DejaVu', '', 6);

    $pdf->AddPage('L');

	$req = mysqli_query($link, "SELECT d.deposit_id, d.deposit_name, d.deposit_percent_year, b.bank_id, b.bank_name, b.bank_inn, b.bank_country, b.bank_reliability, b.bank_assets FROM deposit d LEFT JOIN bank b ON b.bank_id = d.deposit_bank_id WHERE d.deposit_bank_id =".$_GET['depositBankId']);

	$headerBank = array('ID банка:', 'Название банка:', 'ИНН:', 'Страна:', 'Класс надежности:', 'Объем активов:');
	$headerDeposit = array('ID программы депозитов', 'Название программы депозитов', '% годовых');

	$row = mysqli_fetch_row($req);
	for ($i = 0; $i < 6; $i++) {
		$pdf->Cell(strlen($headerBank[$i]), 6, $headerBank[$i]);
		$pdf->Cell(strlen($row[3 + $i]), 6, $row[3 + $i]);
		$pdf->Ln();
	}
	$pdf->Ln();
	$pdf->Ln();

    $pdf->Table($req, $row, $link, $headerDeposit);

    $pdf->Output('deposit.pdf', 'D');
?>