<?
	require('../lib/tfPdf/tfpdf.php');

	class PDF extends tFPDF {

		function Table($req, $link) {
			for($i = 0; $i < mysqli_num_fields($req); $i++)
				$sizeCol[] = 0;

			$countLine = mysqli_num_rows($req);
			$countCol = mysqli_num_fields($req);

			for ($i = 0; $i < $countLine; $i++) {
				$row = mysqli_fetch_array($req, MYSQLI_ASSOC);
				if ($i == 0) {
					$header = array_keys($row);
					$count = 0;
					foreach($header as $col) {
						$data[$i][] = $col;
						if(strlen($col) > $sizeCol[$count])
							$sizeCol[$count] = strlen($col);
						$count++;
					}
				}
				$count = 0;
				foreach($row as $col) {
					$data[$i + 1][] = $col;
					if(strlen($col) > $sizeCol[$count])
						$sizeCol[$count] = strlen($col);
					$count++;

				}
			}

		    for ($i = 0; $i < $countLine + 1; $i++) {
		    	for ($j = 0; $j < $countCol; $j++) {
		    		$this->Cell($sizeCol[$j] + 10, 6, $data[$i][$j], 1);
		    	}
		    	$this->Ln();
			}

		}
	}

	$link = mysqli_connect('localhost', 'u1169180_default', 'c8!_sVJ6') or die ("Невозможно подключиться к серверу");
	mysqli_query($link, 'SET NAMES utf8');
	mysqli_select_db($link, 'u1169180_default') or die ("Нет такой таблицы!");

	$getColName = array_keys($_GET);
	if ($_GET['tableName'] == 'bank')
		$req = mysqli_query($link, "SELECT * FROM ".$_GET['tableName']);
	else
		$req = mysqli_query($link, "SELECT * FROM ".$_GET['tableName']." WHERE ".$getColName[1]."=".$_GET[$getColName[1]]);

	$pdf = new PDF();

	$pdf->AddFont('DejaVu', '', 'DejaVuSansCondensed.ttf', true);
    $pdf->SetFont('DejaVu', '', 6);

    $pdf->AddPage('L');
    $pdf->Table($req, $link);

    $pdf->Output($_GET['tableName'].'.pdf', 'D');
?>