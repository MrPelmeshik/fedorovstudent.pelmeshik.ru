<!DOCTYPE html>
<html>
<head>
	<title>
		Задание 2-3
	</title>
</head>
<body>
	<?
		function OutputArr ($arr, $str) {
			echo '<br><br><h3>' . $str . '</h3>';
			foreach ($arr as $key => $value)
				echo '$cust [&nbsp;' . $key . '&nbsp;] =>&nbsp;' . $value . '<br>';
		}

		$cust = array('cnum'=>'2001', 'cname'=>'Hoffman', 'sity'=>'London', 'snum'=>'1001');
		OutputArr($cust, 'Массив $cust:');

		$cust['rating'] = 100;
		OutputArr($cust, 'Массив $cust с новым ключем:');

		asort($cust);
		OutputArr($cust, 'Отсортирвоанный (asort()) массив $cust:');

		ksort($cust);
		OutputArr($cust, 'Отсортирвоанный (по ключам) массив $cust:');

		sort($cust);
		OutputArr($cust, 'Отсортирвоанный (sort()) массив $cust:');
	?>
</body>
</html>