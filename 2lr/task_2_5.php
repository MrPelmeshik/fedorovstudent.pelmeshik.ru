<!DOCTYPE html>
<html>
<head>
	<title>
		Задание 2-5
	</title>
</head>
<body>
	<?
		function f1 ($u, $t) {
			if ($u < -2)
				return ($u * 2.0);
			else if ($u < 2)
				return (sin($u ** 3.0) + log(abs($t)));
			else 
				return (((cos($u) ** 2.0) + (sin($t **3.0) ** 4.0)) ** (1.0/4.0));
		}

		function f2 ($u, $t) {
			if ($u > 0 && $t > 0)
				return ($u + $t);
			else if ($u <= 0 && $t <= 0)
				return ($u + ($t ** 2));
			else 
				return ($u + $t);
		}

		$a = rand(-50, 50);
		$b = rand(-50, 50);
		echo '$a ->&nbsp;' . $a . 
			'<br>$b ->&nbsp;' . $b;

		echo '<h2>Вариант 14</h2>';
		$z1 = log(abs(f1($a, $b))) + f1(($a ** 2), ($a + $b));
		echo 'Результат:&nbsp;' . $z1;

		echo '<h2>Вариант 1</h2>';
		$z2 = f2(($a - $b), (($b ** 2) - $a)) + f2(($a ** 2) * $b, ($b ** 2));
		echo 'Результат:&nbsp;' . $z2;
	?>
</body>
</html>