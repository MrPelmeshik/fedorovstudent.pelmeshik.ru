<!DOCTYPE html>
<html>
<head>
	<title>
		Задание 2-4
	</title>
</head>
<body>
	<?
		function OutputArr ($arr, $str = 'Array') {
			echo '<h3>' . $str . '</h3>[&nbsp;&nbsp;';
			for ($i = 0; $i < count($arr); $i++)
				echo $arr[$i] . '&nbsp;&nbsp;';
			echo ']<br><br>';
		}

		for ($i = 0; $i < rand(5, 20); $i++)
			$arr[] = rand(-50, 50) / 10;
		OutputArr($arr, 'Массив A(N):');

		echo '<h2>Вариант 14</h2>';
		$arr2 = $arr;
		for ($i =  0; $i < count($arr2); $i++)
			$arr2[$i] = min($arr);
		OutputArr($arr, 'Исходный:');
		OutputArr($arr2, 'Результат:');

		echo '<br><br><h2>Вариант 1</h2>';
		$sum = 0;
		for ($i = 1; $i < count($arr); $i++) {
			$sum += $arr[$i - 1];
			$arr[$i] = $sum;
		}
		OutputArr($arr, 'Результат:');


	?>
</body>
</html>