<!DOCTYPE html>
<html>
<head>
	<title>
		Задача 2-1
	</title>
</head>
<body>
	<?
		function OutputArr($arr, $str, $countElements = false){
			if ($countElements == false) $countElements = count($arr);
			echo '<br><br>' . $str . '&nbsp;->&nbsp;';
			for ($i = 0; $i < $countElements; $i++)
				if(isset($arr[$i])) echo $arr[$i] . ',&nbsp;';
		}

		for ($i = 1; $i <= 10; $i++)
			$treug[$i - 1] = ($i * ($i + 1)) / 2.0;
		OutputArr($treug, '$treug[]');

		for ($i = 1; $i <= 10; $i++)
			$kvd[$i - 1] = $i ** 2;
		OutputArr($kvd, '$kvd[]');

		$rez = array_merge($treug, $kvd);
		OutputArr($rez, '$rez[]');

		sort($rez);
		OutputArr($rez, 'Сортированный $rez[]');

		unset($rez[0]);
		OutputArr($rez, '$rez[] (удален первый элемент)', count($rez) + 1);

		$rez1  = array_unique($rez);
		OutputArr($rez1, '$rez1[]', count($rez) + 1);
	?>
</body>
</html>