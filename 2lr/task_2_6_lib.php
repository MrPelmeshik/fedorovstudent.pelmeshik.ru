<?
	function OutputTask ($key = 0) {
		switch ($key) {
			case 1:
				$str['variant'] = 'Вариант №14';
				$str['task'] = 'Заполнить квадратную матрицу 10x10 последовательными числами от 1 до 100 по спирали';
				break;
			case 2:
				$str['variant'] = 'Вариант №1';
				$str['task'] = 'В матрице Z(n,n) каждый элемент столбца разделить на диагональный, стоящий в том же столбце. Исходный и скорректированный массивы вывести на экран.';
				break;
			default:
				$str['variant'] = 'Вариант № NULL';
				$str['task'] = '< ERROR: invalid key >';
			}

		echo '<h2>' . $str['variant'] . '</h2>' . 
			'<h3>Задание:&nbsp;' . $str['task'] . '</h3>';
	}

	function FillArr (&$arr, $x = 10, $y = 10, $min = -50, $max = 50) {
		for ($i = 0; $i < $x; $i++) {
			for ($j = 0; $j < $y; $j++) {
				$arr[$i][$j] = rand($min, $max);
			}
		}
	}

	function OutputArr ($arr, $str = '') {
		echo '<h3>' . $str . '</h3>';
		echo '<table border=1>';
		for ($i = 0; $i < count($arr); $i++) {
			echo ('<tr>');
			for ($j = 0; $j < count($arr[$i]); $j++) {
				$formatText = sprintf("%.1f", $arr[$i][$j]);
				echo '<td align=center>' . $formatText . '</td>';
			}
		}
		echo '</tr>';
		echo '</table>';
	}

	function ProcessingArr(&$arr) {
		for ($i = 0; $i < count($arr); $i++) {
			for ($j = 0; $j < count($arr[$i]); $j++) {
				$arr[$i][$j] /= $arr[$j][$j];
			}
		}
	}
?>
