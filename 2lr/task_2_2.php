<!DOCTYPE html>
<html>
<head>
	<title>
		Задача 2-2
	</title>
</head>
<body>
	<?
		function OutputArr ($arr, $str = 'Array') {
			echo '<h3>' . $str . '</h3>';
			for ($i = 0; $i < count($arr); $i++)
				echo $arr[$i] . ',&nbsp;';
			echo '<br><br>';
		}


		$count = rand(3, 20);

		for ($i = 0; $i < $count; $i++)
			$arr[$i] = rand(10, 99);
		OutputArr($arr, 'Массив из&nbsp;' . $count . '&nbsp;элементов, заполненный случайными числами:');

		sort($arr);
		OutputArr($arr, 'Массив отсортированный по возрастанию:');

		$arr = array_reverse($arr);
		OutputArr($arr, 'Перевернутый массив:');

		array_pop($arr);
		OutputArr($arr, 'Массив с удаленным последним элементом:');

		$sum = 0;
		for ($i = 0; $i < count($arr); $i++) {
			$sum += $arr[$i];
		}
		echo '<h3>нахождение суммы и количества элементов в массиве:</h3>' .
			'Сумма элементов массива:&nbsp;' . $sum . 
			'<br>Количество элементов:&nbsp;' . count($arr) . '<br><br>';

		echo '<h3>Нахождение средне арифметического:</h3>' .
			'<br>Средне арифметическое:&nbsp;' . ($sum / count($arr)) . '<br><br>';

		echo '<h3>Поиск элемента:</h3>';
		if (in_array(50, $arr)) echo 'Элемент 50 найден!';
		else echo 'Элемент 50 не найден!';
		echo '<br><br>';

		$arr = array_unique($arr);
		OutputArr($arr, 'Фильтрация массива:');
	?>
</body>
</html>