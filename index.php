<!DOCTYPE html>
<html>
<head>
	<title>
		Лабораторные работы "Web-технологии"
	</title>
</head>		
<body>
	<h3>Лабораторные работы "Web-технологии" <br>
	Федоров Илья ИВТ-417</h3>

	<dl>
		<dt><br>Лабораторная работа №1</dt>
			<dd><a href="/1lr/trening_task_1_1.php">Упражнение 1-1</a></dd>
			<dd><a href="/1lr/trening_task_1_2.php">Упражнение 1-2</a></dd>
			<dd><a href="/1lr/trening_task_1_3.php">Упражнение 1-3</a></dd>
			<dd><a href="/1lr/trening_task_1_4.php">Упражнение 1-4</a></dd>
			<dd><a href="/1lr/trening_task_1_6.php">Упражнение 1-6</a></dd>
			<dd><a href="/1lr/trening_task_1_7.php">Упражнение 1-7</a></dd>
			<dd><a href="/1lr/trening_task_1_8.php">Упражнение 1-8</a></dd>
			<dd><a href="/1lr/trening_task_1_9.php">Упражнение 1-9</a></dd>
			<dd><a href="/1lr/trening_task_1_10.php">Упражнение 1-10</a></dd>
			<dd><a href="/1lr/trening_task_1_11.php">Упражнение 1-11</a></dd>
			<dd><a href="/1lr/trening_task_1_12.php">Упражнение 1-12</a></dd>
			<dd><a href="/1lr/trening_task_1_13.php">Упражнение 1-13</a></dd>
			<dd><a href="/1lr/trening_task_1_14.php">Упражнение 1-14</a></dd>
			<dd></dd>
			<dd><a href="/1lr/task_1_1.php">Задача №1-1</a></dd>
			<dd><a href="/1lr/task_1_2.php">Задача №1-2</a></dd>
			<dd><a href="/1lr/task_1_3.php">Задача №1-3</a>
		<dt><br>Лабораторная работа №2</dt>
			<dd><a href="/2lr/task_2_1.php">Задача №2-1</a></dd>
			<dd><a href="/2lr/task_2_2.php">Задача №2-2</a></dd>
			<dd><a href="/2lr/task_2_3.php">Задача №2-3</a></dd>
			<dd><a href="/2lr/task_2_4.php">Задача №2-4</a></dd>
			<dd><a href="/2lr/task_2_5.php">Задача №2-5</a></dd>
			<dd><a href="/2lr/task_2_6.php">Задача №2-6</a>		
		<dt><br>Лабораторная работа №3</dt>
			<dd><a href="/3lr/trening_task_3_1.php">Упражнение 3-1</a></dd>
			<dd><a href="/3lr/trening_task_3_2.php">Упражнение 3-2</a></dd>
			<dd><a href="/3lr/trening_task_3_3.php">Упражнение 3-3</a></dd>
			<dd><a href="/3lr/trening_task_3_4.php">Упражнение 3-4</a></dd>
			<dd><a href="/3lr/trening_task_3_5.php">Упражнение 3-5</a></dd>
			<dd><a href="/3lr/trening_task_3_6.php">Упражнение 3-6</a></dd>
			<dd><a href="/3lr/trening_task_3_7.php">Упражнение 3-7</a></dd>
			<dd></dd>
			<dd><a href="/3lr/task_3_1.php">Задача №3-1</a></dd>
			<dd><a href="/3lr/task_3_2.php">Задача №3-2</a></dd>
			<dd><a href="/3lr/task_3_3.php">Задача №3-3</a></dd>
			<dd><a href="/3lr/task_3_4.php">Задача №3-4</a></dd>
			<dd><a href="/3lr/task_3_5.php">Задача №3-5</a></dd>
			<dd><a href="/3lr/task_3_6.php">Задача №3-6</a>	
		<dt><br>Лабораторная работа №4</dt>
			<dd><a href="/4lr/trening_task_4.php">Упражнение</a></dd>
			<dd><a href="/4lr/task_4.php">Задача</a>
		<dt><br>Лабораторная работа №5</dt>
			<dd><a href="/5lr/task_5_1.php">Задача №5</a>
		<dt><br>Лабораторная работа №6</dt>
			<dd><a href="/6lr/task_6_1.html">Задача №6-1</a></dd>
			<dd><a href="/6lr/task_6_2.html">Задача №6-2</a></dd>
			<dd><a href="/6lr/task_6_3.html">Задача №6-3</a></dd>
			<dd><a href="/6lr/task_6_4_1.html">Задача №6-4-1</a></dd>
			<dd><a href="/6lr/task_6_4_2.html">Задача №6-4-2</a></dd>
			<dd><a href="/6lr/task_6_5.html">Задача №6-5</a></dd>
			<dd><a href="/6lr/task_6_6.html">Задача №6-6</a></dd>
			<dd><a href="/6lr/task_6_7_1.html">Задача №6-7-1</a></dd>
			<dd><a href="/6lr/task_6_7_2.html">Задача №6-7-2</a>
		<dt><br>Лабораторная работа №7</dt>
			<dd><a href="/7lr/task_7_1_1.html">Задача №7-1-1</a></dd>
			<dd><a href="/7lr/task_7_1_2.html">Задача №7-1-2</a></dd>
			<dd><a href="/7lr/task_7_2_1.html">Задача №7-2-1</a></dd>
			<dd><a href="/7lr/task_7_2_2.html">Задача №7-2-2</a></dd>
			<dd><a href="/7lr/task_7_3.html">Задача №7-3</a></dd>
			<dd><a href="/7lr/task_7_4.html">Задача №7-4</a></dd>
			<dd><a href="/7lr/task_7_5.html">Задача №7-5</a></dd>
			<dd><a href="/7lr/task_7_6.html">Задача №7-6</a></dd>
			<dd><a href="/7lr/task_7_7_1.html">Задача №7-7-1</a></dd>
			<dd><a href="/7lr/task_7_7_2.html">Задача №7-7-2</a></dd>
			<dd><a href="/7lr/task_7_8_1.html">Задача №7-8-1</a></dd>
			<dd><a href="/7lr/task_7_8_2.html">Задача №7-8-2</a></dd>
			<dd><a href="/7lr/task_7_9.html">Задача №7-9</a></dd>
		<dt><br>Лабораторная работа №8</dt>
			<dd><a href="/8lr/trening_task_8_1.html">Пример №8-1</a></dd>
			<dd><a href="/8lr/task_8_1.html">Задание №8-1</a></dd>
			<dd><a href="/8lr/task_8_2.html">Задание №8-2</a></dd>
			<dd><a href="/8lr/task_8_3.html">Задание №8-3</a></dd>
			<dd><a href="/8lr/task_8_4.html">Задание №8-4</a></dd>
			<dd><a href="/8lr/task_8_5.html">Задание №8-5</a></dd>
			<dd><a href="/8lr/task_8_6.html">Задание №8-6</a></dd>
			<dd><a href="/8lr/task_8_7.html">Задание №8-7</a>
		<dt><br>Лабораторная работа №9</dt>
			<dd><a href="/9lr/task_9_1.html">Задание №9-1</a></dd>
			<dd><a href="/9lr/task_9_2.html">Задание №9-2</a>
		<dt><br>Лабораторная работа №10</dt>
			<dd><a href="/10lr/task_10.html">Задание №10</a>
		<dt><br>Лабораторная работа №11</dt>
			<dd><a href="/11lr/task_11.html">Задание №11</a>
		<dt><br>Лабораторная работа №12</dt>
			<dd><a href="/12lr/task_12.html">Задание №12</a>
	</dl>


</body>
</html>